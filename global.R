library(shiny)
library(bslib)
library(glue)
library(echarts4r)
library(tidyverse)
library(sf)
library(leaflet)
library(leafgl)
library(colourvalues)
library(arrow)

header_size_int <- 100

upper_plot_style <- function(x){
  x |>
    e_axis(axis = "y", show = FALSE, margin = 0) |>
    e_legend(FALSE) |>
    e_toolbox() |>
    e_toolbox_feature(feature = "brush", type = list("lineX", "clear")) |>
    e_brush(x_index = 1, throttleType = "debounce", throttleDelay = 500) |>
    e_grid(bottom = "20%", top = "10%", left = "10%", right = "10%", containLabel = FALSE) |>
    e_theme("theme_temp")
}

side_plot_style_numbers <- function(x){
  x |>
    #e_axis(axis = "y", show = FALSE, margin = 0) |>
    e_legend(TRUE) |>
    e_toolbox() |>
    e_toolbox_feature(feature = "brush", type = list("lineX", "clear")) |>
    e_brush(x_index = 1, throttleType = "debounce", throttleDelay = 500) |>
    e_grid(bottom = "20%", top = "25%", left = "30%", right = "10%", containLabel = FALSE) |>
    e_theme("theme_total")
}
side_plot_style_2 <- function(x){
  x |>
    #e_axis(axis = "y", show = FALSE, margin = 0) |>
    e_legend(FALSE) |>
    e_toolbox() |>
    e_toolbox_feature(feature = "brush", type = list("lineX", "clear")) |>
    e_brush(x_index = 1, throttleType = "debounce", throttleDelay = 500) |>
    e_grid(bottom = "20%", top = "25%", left = "10%", right = "10%", containLabel = FALSE) |>
    e_theme("theme_attr")
}

communes_france_metropol <- st_read("data/communes_france_metropo.gpkg", as_tibble = TRUE, quiet = TRUE)

communes_name_tbl <- communes_france_metropol %>%
  st_drop_geometry() %>%
  mutate(FullName = glue("{NOM} ({INSEE_COM})")) %>%
  select(INSEE_COM, FullName) %>%
  arrange(FullName)

communes_name_vec <- communes_name_tbl %>% pull(FullName)

st_agr(communes_france_metropol) <- "constant"
centroides_communes_france_metropol <- communes_france_metropol %>%
  st_centroid()



make_line <- function(origin_point, destination_point) {
  coords_origin <- st_coordinates(origin_point)
  coords_end <- st_coordinates(destination_point)
  st_linestring(matrix(c(coords_origin[1], coords_end[1], coords_origin[2], coords_end[2]), 2, 2))
}


prepare_flow_data <- function(data, nb_flows = 1000){
  flow_data <- data %>%
    filter(journey_start_insee != journey_end_insee) %>%
    count(journey_start_insee, journey_end_insee, sort = TRUE) %>%
    ungroup() %>%
    slice_max(n, n = nb_flows, with_ties = FALSE) %>%
    collect()
  
  flow_data_flows<- flow_data %>%
    left_join(centroides_communes_france_metropol %>% select(INSEE_COM), by = c("journey_start_insee" = "INSEE_COM")) %>%
    rename(geometry_start = geom) %>%
    left_join(centroides_communes_france_metropol %>% select(INSEE_COM), by = c("journey_end_insee" = "INSEE_COM")) %>%
    rename(geometry_end = geom) %>%
    filter(!st_is_empty(geometry_start)) %>%
    filter(!st_is_empty(geometry_end)) %>%
    rowwise() %>%
    mutate(line = list(map2(geometry_start, geometry_end, ~make_line(.x, .y)))) %>%
    mutate(line = st_sfc(line)) %>%
    ungroup() %>%
    st_sf(sf_column_name = "line", crs = 2154) %>%
    st_transform(4326) %>%
    arrange(n)
  
  return(flow_data_flows)
}


dist_breaks_labels <- c("0-10km", "10-20km", "20-40km", "40-100km", ">100 km")
dist_breaks_levels <- fct_inorder(dist_breaks_labels, ordered = TRUE)
compute_distance_breaks <- function(data){
distance_breaks <- data %>%
  mutate(distance_km = floor(journey_distance / 5000) * 5) %>%
  mutate(distance_breaks = case_when(
    between(distance_km, 0, 9) ~ dist_breaks_labels[1],
    between(distance_km, 10, 19) ~ dist_breaks_labels[2],
    between(distance_km, 20, 39) ~ dist_breaks_labels[3],
    between(distance_km, 40, 99) ~ dist_breaks_labels[4],
    distance_km >= 100 ~ dist_breaks_labels[5],
    TRUE ~ "Autre"
  )) %>%
  count(distance_breaks) %>%
  ungroup() %>%
  collect() %>%
  mutate(distance_breaks = factor(distance_breaks, levels = dist_breaks_levels)) %>%
  arrange(distance_breaks) %>%
  mutate(id = row_number())

return(distance_breaks)
}
tmp_compute_distance_breaks <- function(data){
  data %>%
    mutate(distance_km = floor(journey_distance / 5000) * 5) %>%
    mutate(distance_breaks = case_when(
      between(distance_km, 0, 9) ~ dist_breaks_labels[1],
      between(distance_km, 10, 19) ~ dist_breaks_labels[2],
      between(distance_km, 20, 39) ~ dist_breaks_labels[3],
      between(distance_km, 40, 99) ~ dist_breaks_labels[4],
      distance_km >= 100 ~ dist_breaks_labels[5],
      TRUE ~ "Autre"
    ))
}

duration_breaks_labels <- c("0-10min", "10-20min", "20-45min", "45min-1h30", ">1h30")
duration_breaks_levels <- fct_inorder(duration_breaks_labels, ordered = TRUE)
compute_duration_breaks <- function(data){
  duration_breaks <- data %>%
    mutate(duration_breaks = case_when(
      between(journey_duration, 0, 9.9) ~ duration_breaks_labels[1],
      between(journey_duration, 10, 19.9) ~ duration_breaks_labels[2],
      between(journey_duration, 20, 44.9) ~ duration_breaks_labels[3],
      between(journey_duration, 45, 89.9) ~ duration_breaks_labels[4],
      journey_duration >= 90 ~ duration_breaks_labels[5],
      TRUE ~ "Autre"
    )) %>%
    count(duration_breaks) %>%
    ungroup() %>%
    collect() %>%
    mutate(duration_breaks = factor(duration_breaks, levels = duration_breaks_levels)) %>%
    arrange(duration_breaks) %>%
    mutate(id = row_number())
  
  return(duration_breaks)
}
#compute_duration_breaks(raw_data)
tmp_compute_duration_breaks <- function(data){
  data %>%
    mutate(duration_breaks = case_when(
      between(journey_duration, 0, 9.9) ~ duration_breaks_labels[1],
      between(journey_duration, 10, 19.9) ~ duration_breaks_labels[2],
      between(journey_duration, 20, 44.9) ~ duration_breaks_labels[3],
      between(journey_duration, 45, 89.9) ~ duration_breaks_labels[4],
      journey_duration >= 90 ~ duration_breaks_labels[5],
      TRUE ~ "Autre"
    ))
}


speed_breaks_labels <- c("0-20kmh", "20-40kmh", "40-80kmh", "80-110kmh", ">110kmh")
speed_breaks_levels <- fct_inorder(speed_breaks_labels, ordered = TRUE)
compute_speed_breaks <- function(data){
  speed_breaks <- data %>%
    mutate(speed = (journey_distance / 1000) / (journey_duration / 60)) %>%
    mutate(speed_breaks = case_when(
      between(speed, 0, 19.9) ~ speed_breaks_labels[1],
      between(speed, 20, 39.9) ~ speed_breaks_labels[2],
      between(speed, 40, 79.9) ~ speed_breaks_labels[3],
      between(speed, 80, 109.9) ~ speed_breaks_labels[4],
      speed >= 110 ~ speed_breaks_labels[5],
      TRUE ~ "Autre"
    )) %>%
    count(speed_breaks) %>%
    ungroup() %>%
    collect() %>%
    mutate(speed_breaks = factor(speed_breaks, levels = speed_breaks_levels)) %>%
    filter(!is.na(speed_breaks)) %>%
    arrange(speed_breaks) %>%
    mutate(id = row_number())
  
  return(speed_breaks)
}
#compute_speed_breaks(raw_data)
tmp_compute_speed_breaks <- function(data){
  data %>%
    mutate(speed = (journey_distance / 1000) / (journey_duration / 60)) %>%
    mutate(speed_breaks = case_when(
      between(speed, 0, 19.9) ~ speed_breaks_labels[1],
      between(speed, 20, 39.9) ~ speed_breaks_labels[2],
      between(speed, 40, 79.9) ~ speed_breaks_labels[3],
      between(speed, 80, 109.9) ~ speed_breaks_labels[4],
      speed >= 110 ~ speed_breaks_labels[5],
      TRUE ~ "Autre"
    ))
}

# this_data <- communes_france_metropol %>%
#   st_cast("POLYGON") %>%
#   #st_simplify(preserveTopology = TRUE, dTolerance = 100) %>%
#   st_transform(4326)
# leaflet(this_data) %>%
#   addProviderTiles(providers$CartoDB.DarkMatter) %>%
#   addGlPolygons(data = this_data)
