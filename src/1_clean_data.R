library(tidyverse)
library(glue)
library(lubridate)

csv_files <- list.files("data/", pattern = "*.csv")

csv_listing <- list()
for (this_file in csv_files){
  foo <- read_delim(glue("data/{this_file}"), delim = ";",
                    col_types = cols(.default = col_character())) %>%
    mutate(across(ends_with("_datetime"), ~as_datetime(.x, tz = "Europe/Paris"))) %>%
    mutate(across(ends_with("_lat"), ~as.numeric(.x))) %>%
    mutate(across(ends_with("_lon"), ~as.numeric(.x))) %>%
    mutate(journey_distance = as.numeric(journey_distance),
           journey_duration = as.numeric(journey_duration))
  csv_listing[[this_file]] <- foo
}

complete_dataset <- bind_rows(csv_listing)


write_rds(complete_dataset, "data/rpc_complete_2022-11.rds")
