# RPC Shiny MAGISAR9
<br/>
![app screenshot](www/app_rpc_1.png)
<br/>
<br/>
<br/>

### Application et présentation basée sur les données du [Registre de Preuve de Covoiturage](https://www.data.gouv.fr/fr/datasets/trajets-realises-en-covoiturage-registre-de-preuve-de-covoiturage/), à l'occasion de la journée d'étude "[La boîte à outils de cartographie et de géovisualisation de données : regards croisés de chercheurs](https://github.com/magisAR9/JEGeovizRennes)" organisée à Rennes le 06/01/2023.
<br/>
<br/>
<br/>

<details open>
<summary> <span title='Click to Expand'> Current session info </span> </summary>

```r

─ Session info ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 setting  value
 version  R version 4.2.2 Patched (2022-11-10 r83330)
 os       Ubuntu 20.04.5 LTS
 system   x86_64, linux-gnu
 ui       RStudio
 language (EN)
 collate  fr_FR.UTF-8
 ctype    fr_FR.UTF-8
 tz       Europe/Paris
 date     2023-01-06
 rstudio  2022.12.0+353 Elsbeth Geranium (desktop)
 pandoc   2.5 @ /usr/bin/pandoc

─ Packages ───────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
 package       * version date (UTC) lib source
 arrow         * 10.0.1  2022-12-06 [1] RSPM (R 4.2.0)
 assertthat      0.2.1   2019-03-21 [1] RSPM (R 4.2.0)
 backports       1.4.1   2021-12-13 [1] RSPM (R 4.2.0)
 bit             4.0.5   2022-11-15 [1] RSPM (R 4.2.0)
 bit64           4.0.5   2020-08-30 [1] RSPM (R 4.2.0)
 broom           1.0.2   2022-12-15 [1] CRAN (R 4.2.2)
 bslib         * 0.4.1   2022-11-02 [1] RSPM (R 4.2.0)
 cachem          1.0.6   2021-08-19 [1] RSPM (R 4.2.0)
 cellranger      1.1.0   2016-07-27 [1] RSPM (R 4.2.0)
 class           7.3-20  2022-01-13 [4] CRAN (R 4.1.2)
 classInt        0.4-8   2022-09-29 [1] RSPM (R 4.2.0)
 cli             3.4.1   2022-09-23 [1] RSPM (R 4.2.0)
 clipr           0.8.0   2022-02-22 [1] CRAN (R 4.2.2)
 colorspace      2.0-3   2022-02-21 [1] CRAN (R 4.2.0)
 colourvalues  * 0.3.7   2020-12-07 [1] CRAN (R 4.2.2)
 crayon          1.5.2   2022-09-29 [1] RSPM (R 4.2.0)
 crosstalk       1.2.0   2021-11-04 [1] RSPM (R 4.2.0)
 DBI             1.1.3   2022-06-18 [1] RSPM (R 4.2.0)
 dbplyr          2.2.1   2022-06-27 [1] RSPM (R 4.2.0)
 desc            1.4.2   2022-09-08 [1] RSPM (R 4.2.0)
 details         0.3.0   2022-03-27 [1] CRAN (R 4.2.2)
 digest          0.6.31  2022-12-11 [1] RSPM (R 4.2.0)
 dplyr         * 1.0.10  2022-09-01 [1] RSPM (R 4.2.0)
 e1071           1.7-12  2022-10-24 [1] RSPM (R 4.2.0)
 echarts4r     * 0.4.4   2022-05-28 [1] CRAN (R 4.2.2)
 ellipsis        0.3.2   2021-04-29 [1] CRAN (R 4.2.0)
 fansi           1.0.3   2022-03-24 [1] CRAN (R 4.2.0)
 fastmap         1.1.0   2021-01-25 [1] RSPM (R 4.2.0)
 forcats       * 0.5.2   2022-08-19 [1] RSPM (R 4.2.0)
 fs              1.5.2   2021-12-08 [1] RSPM (R 4.2.0)
 gargle          1.2.1   2022-09-08 [1] RSPM (R 4.2.0)
 generics        0.1.3   2022-07-05 [1] RSPM (R 4.2.0)
 ggplot2       * 3.4.0   2022-11-04 [1] CRAN (R 4.2.2)
 glue          * 1.6.2   2022-02-24 [1] CRAN (R 4.2.0)
 googledrive     2.0.0   2021-07-08 [1] RSPM (R 4.2.0)
 googlesheets4   1.0.1   2022-08-13 [1] RSPM (R 4.2.0)
 gtable          0.3.1   2022-09-01 [1] RSPM (R 4.2.0)
 haven           2.5.1   2022-08-22 [1] RSPM (R 4.2.0)
 hms             1.1.2   2022-08-19 [1] RSPM (R 4.2.0)
 htmltools       0.5.4   2022-12-07 [1] RSPM (R 4.2.0)
 htmlwidgets     1.6.0   2022-12-15 [1] CRAN (R 4.2.2)
 httpuv          1.6.7   2022-12-14 [1] RSPM (R 4.2.0)
 httr            1.4.4   2022-08-17 [1] RSPM (R 4.2.0)
 jquerylib       0.1.4   2021-04-26 [1] RSPM (R 4.2.0)
 jsonlite        1.8.4   2022-12-06 [1] RSPM (R 4.2.0)
 KernSmooth      2.23-20 2021-05-03 [4] CRAN (R 4.0.5)
 knitr           1.41    2022-11-18 [1] RSPM (R 4.2.0)
 later           1.3.0   2021-08-18 [1] RSPM (R 4.2.0)
 leafgl        * 0.1.1   2020-06-28 [1] CRAN (R 4.2.1)
 leaflet       * 2.1.1   2022-03-23 [1] RSPM (R 4.2.0)
 lifecycle       1.0.3   2022-10-07 [1] RSPM (R 4.2.0)
 lubridate       1.9.0   2022-11-06 [1] RSPM (R 4.2.0)
 magrittr        2.0.3   2022-03-30 [1] CRAN (R 4.2.0)
 mime            0.12    2021-09-28 [1] RSPM (R 4.2.0)
 modelr          0.1.10  2022-11-11 [1] RSPM (R 4.2.0)
 munsell         0.5.0   2018-06-12 [1] CRAN (R 4.2.0)
 pillar          1.8.1   2022-08-19 [1] RSPM (R 4.2.0)
 pkgconfig       2.0.3   2019-09-22 [1] CRAN (R 4.2.0)
 png             0.1-8   2022-11-29 [1] RSPM (R 4.2.0)
 promises        1.2.0.1 2021-02-11 [1] RSPM (R 4.2.0)
 proxy           0.4-27  2022-06-09 [1] RSPM (R 4.2.0)
 purrr         * 0.3.5   2022-10-06 [1] RSPM (R 4.2.0)
 R6              2.5.1   2021-08-19 [1] CRAN (R 4.2.0)
 Rcpp            1.0.9   2022-07-08 [1] RSPM (R 4.2.0)
 readr         * 2.1.3   2022-10-01 [1] RSPM (R 4.2.0)
 readxl          1.4.1   2022-08-17 [1] RSPM (R 4.2.0)
 reprex          2.0.2   2022-08-17 [1] RSPM (R 4.2.0)
 rlang           1.0.6   2022-09-24 [1] RSPM (R 4.2.0)
 rprojroot       2.0.3   2022-04-02 [1] CRAN (R 4.2.0)
 rstudioapi      0.14    2022-08-22 [1] RSPM (R 4.2.0)
 rvest           1.0.3   2022-08-19 [1] RSPM (R 4.2.0)
 sass            0.4.4   2022-11-24 [1] RSPM (R 4.2.0)
 scales          1.2.1   2022-08-20 [1] RSPM (R 4.2.0)
 sessioninfo     1.2.2   2021-12-06 [1] CRAN (R 4.2.2)
 sf            * 1.0-9   2022-11-08 [1] CRAN (R 4.2.2)
 shiny         * 1.7.4   2022-12-15 [1] CRAN (R 4.2.2)
 stringi         1.7.8   2022-07-11 [1] RSPM (R 4.2.0)
 stringr       * 1.5.0   2022-12-02 [1] RSPM (R 4.2.0)
 tibble        * 3.1.8   2022-07-22 [1] RSPM (R 4.2.0)
 tidyr         * 1.2.1   2022-09-08 [1] RSPM (R 4.2.0)
 tidyselect      1.2.0   2022-10-10 [1] RSPM (R 4.2.0)
 tidyverse     * 1.3.2   2022-07-18 [1] RSPM (R 4.2.0)
 timechange      0.1.1   2022-11-04 [1] RSPM (R 4.2.0)
 tzdb            0.3.0   2022-03-28 [1] RSPM (R 4.2.0)
 units           0.8-1   2022-12-10 [1] RSPM (R 4.2.0)
 utf8            1.2.2   2021-07-24 [1] CRAN (R 4.2.0)
 vctrs           0.5.1   2022-11-16 [1] RSPM (R 4.2.0)
 withr           2.5.0   2022-03-03 [1] CRAN (R 4.2.0)
 xfun            0.35    2022-11-16 [1] RSPM (R 4.2.0)
 xml2            1.3.3   2021-11-30 [1] RSPM (R 4.2.0)
 xtable          1.8-4   2019-04-21 [1] RSPM (R 4.2.0)

 [1] /home/robin/R/x86_64-pc-linux-gnu-library/4.2
 [2] /usr/local/lib/R/site-library
 [3] /usr/lib/R/site-library
 [4] /usr/lib/R/library

──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────

```

</details>
<br>

